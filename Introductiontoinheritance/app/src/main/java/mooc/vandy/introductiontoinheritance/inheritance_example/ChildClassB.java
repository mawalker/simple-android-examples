package mooc.vandy.introductiontoinheritance.inheritance_example;

/**
 * Created by mike on 10/5/15.
 */
public class ChildClassB extends ParentClass {

    private static String CLASS_NAME_STRING = "CHILD CLASS B";

    public String getName(){
        return CLASS_NAME_STRING + "\n";
    }
}
