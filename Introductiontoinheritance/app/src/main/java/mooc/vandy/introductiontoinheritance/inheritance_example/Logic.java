package mooc.vandy.introductiontoinheritance.inheritance_example;

/**
 * Created by mike on 10/5/15.
 */
public class Logic {



    public String GetTextNoArray(){
        String returnValueString = "";

        ParentClass one = new ParentClass();
        ParentClass two = new ParentClass();
        ParentClass three = new ChildClassA();
        ParentClass four = new ChildClassB();
        ParentClass five = new ParentClass();

        returnValueString = returnValueString + one.getName();
        returnValueString += two.getName();
        returnValueString += three.getName();
        returnValueString += four.getName();
        returnValueString += five.getName();

        return returnValueString;
    }


    public String GetText(){
        String returnValueString = "";

        ParentClass[] instances = new ParentClass[5];

        instances[0] = new ParentClass();
        instances[1] = new ParentClass();
        instances[2] = new ChildClassA();
        instances[3] = new ChildClassB();
        instances[4] = new ParentClass();

        returnValueString = returnValueString + instances[0].getName();
        returnValueString += instances[1].getName();
        returnValueString += instances[2].getName();
        returnValueString += instances[3].getName();
        returnValueString += instances[4].getName();

        return returnValueString;
    }
}
