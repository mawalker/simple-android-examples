package mooc.vandy.introductiontoinheritance.required;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import mooc.vandy.introductiontoinheritance.R;
import mooc.vandy.introductiontoinheritance.inheritance_example.Logic;

public class MainActivity extends AppCompatActivity {

    private TextView outputTextView;

    /**
     * Part of the Android Life-Cycle. Called when MainActivity is created.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupUserInterface();
    }

    /**
     * This method does the one-time setup of all UserInterface related objects.
     */
    private void setupUserInterface(){
        setContentView(R.layout.activity_main);
        outputTextView = (TextView) findViewById(R.id.outputtextview);
    }

    /**
     * This method will be called when the button is clicked
     * <p>
     *     the /res/layout/activity_main.xml defines a Button with the name 'button' as having the
     *     'onclick' parameter with the value of 'buttonPressed'. This instructs Android to call
     *     this method when that button is pressed.
     *
     * </p>
     * @param view
     */
    public void buttonPressed(View view){
        Logic exampleLogic = new Logic();
        outputTextView.setText(exampleLogic.GetText());
    }
}
