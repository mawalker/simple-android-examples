package mooc.vandy.introductiontoinheritance.inheritance_example;

/**
 * Created by mike on 10/5/15.
 */
public class ChildClassA extends ParentClass {

    private static String CLASS_NAME_STRING = "CHILD CLASS A";

    public String getName(){
        return CLASS_NAME_STRING + "\n";
    }
}
