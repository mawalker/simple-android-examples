package mooc.vandy.introductiontoinheritance.inheritance_example;

/**
 * Created by mike on 10/5/15.
 */
public class ParentClass {

    private static String CLASS_NAME_STRING = "BASE CLASS";

    public String getName(){
        return CLASS_NAME_STRING + "\n";
    }

}
